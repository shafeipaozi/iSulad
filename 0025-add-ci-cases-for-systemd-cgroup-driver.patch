From f5f100f5b244be2debebe815aaed3afad8950daf Mon Sep 17 00:00:00 2001
From: jikai <jikai11@huawei.com>
Date: Tue, 6 Feb 2024 17:33:17 +0800
Subject: [PATCH 25/26] add ci cases for systemd cgroup driver

Signed-off-by: jikai <jikai11@huawei.com>
---
 .../container_cases/systemd_cgroup.sh         | 80 +++++++++++++++++++
 test/mocks/isulad_config_mock.cc              |  8 ++
 test/mocks/isulad_config_mock.h               |  1 +
 test/specs/specs/specs_ut.cc                  | 49 ++++++++++++
 4 files changed, 138 insertions(+)
 create mode 100755 CI/test_cases/container_cases/systemd_cgroup.sh

diff --git a/CI/test_cases/container_cases/systemd_cgroup.sh b/CI/test_cases/container_cases/systemd_cgroup.sh
new file mode 100755
index 00000000..ac1288e1
--- /dev/null
+++ b/CI/test_cases/container_cases/systemd_cgroup.sh
@@ -0,0 +1,80 @@
+#!/bin/bash
+#
+# attributes: isulad systemd cgroup run
+# concurrent: NO
+# spend time: 18
+
+#######################################################################
+##- Copyright (c) Huawei Technologies Co., Ltd. 2020. All rights reserved.
+# - iSulad licensed under the Mulan PSL v2.
+# - You can use this software according to the terms and conditions of the Mulan PSL v2.
+# - You may obtain a copy of Mulan PSL v2 at:
+# -     http://license.coscl.org.cn/MulanPSL2
+# - THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
+# - IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
+# - PURPOSE.
+# - See the Mulan PSL v2 for more details.
+##- @Description:CI
+##- @Author: jikai
+##- @Create: 2024-02-05
+#######################################################################
+
+curr_path=$(dirname $(readlink -f "$0"))
+data_path=$(realpath $curr_path/../data)
+source ../helpers.sh
+
+function test_systemd_cgroup()
+{
+    local ret=0
+    local runtime=$1
+    local image="busybox"
+
+    local test="systemd cgroup driver test with (${runtime})=> (${FUNCNAME[@]})"
+    msg_info "${test} starting..."
+
+    check_valgrind_log
+    [[ $? -ne 0 ]] && msg_err "${FUNCNAME[0]}:${LINENO} - stop isulad failed" && ((ret++))
+
+    start_isulad_with_valgrind --systemd-cgroup
+    [[ $? -ne 0 ]] && msg_err "${FUNCNAME[0]}:${LINENO} - start isulad failed" && ((ret++))
+
+    cid1=$(isula run -tid --runtime $runtime -m 10M $image /bin/sh)
+    [[ $? -ne 0 ]] && msg_err "${FUNCNAME[0]}:${LINENO} - start container failed" && ((ret++))
+    cat /sys/fs/cgroup/memory/system.slice/isulad-$cid1.scope/memory.limit_in_bytes | grep ^10485760$
+    [[ $? -ne 0 ]] && msg_err "${FUNCNAME[0]}:${LINENO} - check memory limit failed" && ((ret++))
+
+    cid2=$(isula run -tid --runtime $runtime --cgroup-parent /test $image /bin/sh)
+    [[ $? -eq 0 ]] && msg_err "${FUNCNAME[0]}:${LINENO} - start container failed" && ((ret++))
+
+    cid3=$(isula run -tid --runtime $runtime -m 10M --cgroup-parent test-a-b.slice $image /bin/sh)
+    [[ $? -ne 0 ]] && msg_err "${FUNCNAME[0]}:${LINENO} - start container failed" && ((ret++))
+    cat /sys/fs/cgroup/memory/test.slice/test-a.slice/test-a-b.slice/isulad-$cid3.scope/memory.limit_in_bytes | grep ^10485760$
+    [[ $? -ne 0 ]] && msg_err "${FUNCNAME[0]}:${LINENO} - check memory limit failed" && ((ret++))
+
+    isula rm -f $cid1 $cid2 $cid3
+    [[ $? -ne 0 ]] && msg_err "${FUNCNAME[0]}:${LINENO} - rm container failed" && ((ret++))
+
+    check_valgrind_log
+    [[ $? -ne 0 ]] && msg_err "${FUNCNAME[0]}:${LINENO} - stop isulad failed" && ((ret++))
+
+    start_isulad_with_valgrind
+    [[ $? -ne 0 ]] && msg_err "${FUNCNAME[0]}:${LINENO} - start isulad failed" && ((ret++))
+
+    rm -rf $ulimitlog
+
+    msg_info "${test} finished with return ${ret}..."
+    return ${ret}
+}
+
+declare -i ans=0
+
+for element in ${RUNTIME_LIST[@]};
+do
+    # lcr does not support systemd cgroup driver
+    if [ "$element" == "lcr" ];then
+        continue
+    fi
+    test_systemd_cgroup $element || ((ans++))
+done
+
+show_result ${ans} "${curr_path}/${0}"
diff --git a/test/mocks/isulad_config_mock.cc b/test/mocks/isulad_config_mock.cc
index 7ba4fa57..65b00563 100644
--- a/test/mocks/isulad_config_mock.cc
+++ b/test/mocks/isulad_config_mock.cc
@@ -210,3 +210,11 @@ char *conf_get_isulad_loglevel(void)
     }
     return nullptr;
 }
+
+bool conf_get_systemd_cgroup(void)
+{
+    if (g_isulad_conf_mock != nullptr) {
+        return g_isulad_conf_mock->ConfGetSystemdCgroup();
+    }
+    return false;
+}
diff --git a/test/mocks/isulad_config_mock.h b/test/mocks/isulad_config_mock.h
index 6793fa51..d59c5938 100644
--- a/test/mocks/isulad_config_mock.h
+++ b/test/mocks/isulad_config_mock.h
@@ -45,6 +45,7 @@ public:
     MOCK_METHOD0(ConfGetSandboxStatePath, char *(void));
     MOCK_METHOD0(ConfGetEngineLogFile, char *(void));
     MOCK_METHOD0(ConfGetIsuladLogLevel, char *(void));
+    MOCK_METHOD0(ConfGetSystemdCgroup, bool(void));
 };
 
 void MockIsuladConf_SetMock(MockIsuladConf *mock);
diff --git a/test/specs/specs/specs_ut.cc b/test/specs/specs/specs_ut.cc
index 47e4ca6e..6c42216d 100644
--- a/test/specs/specs/specs_ut.cc
+++ b/test/specs/specs/specs_ut.cc
@@ -319,6 +319,7 @@ TEST_F(SpecsUnitTest, test_merge_container_cgroups_path_2)
     ASSERT_TRUE(host_spec != nullptr);
 
     EXPECT_CALL(m_isulad_conf, GetCgroupParent()).WillRepeatedly(Invoke(invoke_conf_get_isulad_cgroup_parent_null));
+    EXPECT_CALL(m_isulad_conf, ConfGetSystemdCgroup()).WillRepeatedly(Return(false));
 
     merged_cp = merge_container_cgroups_path("123", host_spec);
     ASSERT_NE(merged_cp, nullptr);
@@ -347,6 +348,7 @@ TEST_F(SpecsUnitTest, test_merge_container_cgroups_path_3)
     host_spec->cgroup_parent = util_strdup_s("/test");
 
     EXPECT_CALL(m_isulad_conf, GetCgroupParent()).WillRepeatedly(Invoke(invoke_conf_get_isulad_cgroup_parent_null));
+    EXPECT_CALL(m_isulad_conf, ConfGetSystemdCgroup()).WillRepeatedly(Return(false));
 
     merged_cp = merge_container_cgroups_path("123", host_spec);
     ASSERT_NE(merged_cp, nullptr);
@@ -373,6 +375,7 @@ TEST_F(SpecsUnitTest, test_merge_container_cgroups_path_4)
     ASSERT_TRUE(host_spec != nullptr);
 
     EXPECT_CALL(m_isulad_conf, GetCgroupParent()).WillRepeatedly(Invoke(invoke_conf_get_isulad_cgroup_parent));
+    EXPECT_CALL(m_isulad_conf, ConfGetSystemdCgroup()).WillRepeatedly(Return(false));
 
     merged_cp = merge_container_cgroups_path("123", host_spec);
     ASSERT_NE(merged_cp, nullptr);
@@ -401,6 +404,7 @@ TEST_F(SpecsUnitTest, test_merge_container_cgroups_path_5)
     host_spec->cgroup_parent = util_strdup_s("/test");
 
     EXPECT_CALL(m_isulad_conf, GetCgroupParent()).WillRepeatedly(Invoke(invoke_conf_get_isulad_cgroup_parent));
+    EXPECT_CALL(m_isulad_conf, ConfGetSystemdCgroup()).WillRepeatedly(Return(false));
 
     merged_cp = merge_container_cgroups_path("123", host_spec);
     ASSERT_NE(merged_cp, nullptr);
@@ -414,6 +418,51 @@ TEST_F(SpecsUnitTest, test_merge_container_cgroups_path_5)
     testing::Mock::VerifyAndClearExpectations(&m_isulad_conf);
 }
 
+// systemd cgroup test
+TEST_F(SpecsUnitTest, test_merge_container_cgroups_path_6)
+{
+    oci_runtime_spec *oci_spec = nullptr;
+    host_config *host_spec = nullptr;
+    char *merged_cp = nullptr;
+
+    oci_spec = (oci_runtime_spec *)util_common_calloc_s(sizeof(oci_runtime_spec));
+    ASSERT_TRUE(oci_spec != nullptr);
+
+    host_spec = (host_config *)util_common_calloc_s(sizeof(host_config));
+    ASSERT_TRUE(host_spec != nullptr);
+
+    EXPECT_CALL(m_isulad_conf, GetCgroupParent()).WillRepeatedly(Invoke(invoke_conf_get_isulad_cgroup_parent_null));
+    EXPECT_CALL(m_isulad_conf, ConfGetSystemdCgroup()).WillRepeatedly(Return(true));
+
+    merged_cp = merge_container_cgroups_path("123", host_spec);
+    ASSERT_NE(merged_cp, nullptr);
+    ASSERT_STREQ(merged_cp, "system.slice:isulad:123");
+    free(merged_cp);
+
+    host_spec->cgroup_parent = util_strdup_s("/test");
+    merged_cp = merge_container_cgroups_path("123", host_spec);
+    ASSERT_EQ(merged_cp, nullptr);
+    free(host_spec->cgroup_parent);
+
+    host_spec->cgroup_parent = util_strdup_s("test.slice");
+    merged_cp = merge_container_cgroups_path("123", host_spec);
+    ASSERT_NE(merged_cp, nullptr);
+    ASSERT_STREQ(merged_cp, "test.slice:isulad:123");
+    free(merged_cp);
+    free(host_spec->cgroup_parent);
+
+    host_spec->cgroup_parent = util_strdup_s("test/test-a/test-a-b.slice");
+    merged_cp = merge_container_cgroups_path("123", host_spec);
+    ASSERT_NE(merged_cp, nullptr);
+    ASSERT_STREQ(merged_cp, "test-a-b.slice:isulad:123");
+
+    free_oci_runtime_spec(oci_spec);
+    free_host_config(host_spec);
+    free(merged_cp);
+
+    testing::Mock::VerifyAndClearExpectations(&m_isulad_conf);
+}
+
 TEST_F(SpecsUnitTest, test_update_oci_container_cgroups_path)
 {
     parser_error err = nullptr;
-- 
2.25.1

